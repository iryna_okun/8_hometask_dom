 // не получилось подгрузить через этот код. Открыла через нижний код в мозилле

 /*  window.onload = () => {

    const url = 'https://rickandmortyapi.com/api/character'
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map(character => {
          //all needed data is listed below as an entity 
          let created = character.created;
                species = character.species ;
                img = character.image;
                episodes = character.episode;
                name = character.name;
		location = character.location;
            //create element
            //append element
        });
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });
}

*/
// мозилла

fetch('https://rickandmortyapi.com/api/character/')
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    console.log(data);
  });

  window.onscroll = function() {myFunction()};

// работа с DOM API


/* 1. создаем новый элемент CARD1

(function () {
  var card1 = document.createElement('div') 

  console.log (card1);
}) ();

*/

// Вставляем его (card1) на страницу
/*

(function () {
  var card1 = document.createElement('div') 

content = document.createTextNode ("CARD1")
card1.appendChild(content);
console.log(card1);
}) ();

 

// Выводим на страницу текст имя первой карточки
(function () {
  var card1 = document.createElement('div') 

content = document.createTextNode ("CARD1")


CARDSDIV = document.getElementById("CARDS")

card1.appendChild(content);
card1.id = 'pervuj';

CARDSDIV.parentNode.appendChild(card1);

}) ();
*/

//СОЗДАНИЕ ПЕРВОЙ КАРТЫ

var div1 = document.querySelector('CARDS')
var card1 = document.createElement('div')
// card1.className = 'card1'


CARDS.appendChild(card1)

card1.id = 'pervuj';

card1.innerHTML = "This is 1st card info"

//создание первого параграфа в первой карте - имени

var par1 = document.querySelector('pervuj')
var name1 = document.createElement('P')

pervuj.appendChild(name1)

name1.id = 'namefirst';
name1.style.color = "orange"


name1.innerHTML = "Rick Sanchez"

// создание второго параграфа к первой карточке species

var par2 = document.querySelector('pervuj')
var species1 = document.createElement('P')
pervuj.appendChild(species1)
species1.id = 'speciesfirst';
species1.innerHTML = "Human"

// создание третьего параграфа к первой карточке location

var par3 = document.querySelector('pervuj')
var location1 = document.createElement('P')
pervuj.appendChild(location1)
location1.id = 'locationfirst';
location1.innerHTML = "Earth (Replacement Dimension)"

// создание четвертого параграфа к первой карточке created

var par4 = document.querySelector('pervuj')
var created1 = document.createElement('P')
pervuj.appendChild(created1)
created1.id = 'createdfirst';
created1.innerHTML = "2017-11-04T18:48:46.250Z"

// создание пятого параграфа к первой карточке episode

var par5 = document.querySelector('pervuj')
var episode1 = document.createElement('P')
pervuj.appendChild(episode1)
episode1.id = 'episodefirst';
episode1.innerHTML = "36"


// создание шестого параграфа в первой карте - картинки 

var img1 = document.querySelector('pervuj')
var foto1 = document.createElement('img')

pervuj.appendChild(foto1)

foto1.id = 'fotofirst';

foto1.innerHTML = "<img name='first' src='https://rickandmortyapi.com/api/character/avatar/1.jpeg' style='height:100%; width:100%;'>";



//СОЗДАНИЕ ВТОРОЙ КАРТЫ

var div1 = document.querySelector('CARDS')
var card2 = document.createElement('div')
// card2.className = 'card2'
CARDS.appendChild(card2)
card2.id = 'vtoroj';
card2.innerHTML = "This is 2nd card info"

//создание первого параграфа во второй карте - имени

var par21 = document.querySelector('vtoroj')
var name21 = document.createElement('P')
vtoroj.appendChild(name21)
name21.id = 'namesecond';
name21.style.color = "blue"
name21.innerHTML = "Morty Smith"

// создание второго параграфа во второй карточке species

var par2 = document.querySelector('vtoroj')
var species2 = document.createElement('P')
vtoroj.appendChild(species2)
species2.id = 'speciessecond';
species2.innerHTML = "Human"

// создание третьего параграфа во второй карточке location

var par3 = document.querySelector('vtoroj')
var location2 = document.createElement('P')
vtoroj.appendChild(location2)
location2.id = 'locationsecond';
location2.innerHTML = "Earth (Replacement Dimension)"

// создание четвертого параграфа во второй карточке created

var par4 = document.querySelector('vtoroj')
var created2 = document.createElement('P')
vtoroj.appendChild(created2)
created2.id = 'createdsecond';
created2.innerHTML = "2017-11-04T18:50:21.651Z"

// создание пятого параграфа во второй карточке episode

var par5 = document.querySelector('vtoroj')
var episode2 = document.createElement('P')
vtoroj.appendChild(episode2)
episode2.id = 'episodesecond';
episode2.innerHTML = "36"

// создание шестого параграфа во второй карте - картинки 

var img2 = document.querySelector('vtoroj')
var foto2 = document.createElement('img')

vtoroj.appendChild(foto2)

foto2.id = 'fotosecond';

foto2.innerHTML = "<img name='second' src='https://rickandmortyapi.com/api/character/avatar/2.jpeg' style='height:100%; width:100%;'>";



//СОЗДАНИЕ ТРЕТЬЕЙ КАРТЫ

var div3 = document.querySelector('CARDS')
var card3 = document.createElement('div')
// card3.className = 'card3'


CARDS.appendChild(card3)

card3.id = 'tretij';

card3.innerHTML = "This is 3rd card info"


//создание первого параграфа в третьей карте - имени

var par31 = document.querySelector('tretij')
var name31 = document.createElement('P')
tretij.appendChild(name31)
name31.id = 'namethird';
name31.style.color = "green"
name31.innerHTML = "Summer Smith"

// создание второго параграфа в третьей карточке species

var par32 = document.querySelector('tretij')
var species3 = document.createElement('P')
tretij.appendChild(species3)
species3.id = 'speciesthird';
species3.innerHTML = "Human"

// создание третьего параграфа в третьей карточке location

var par33 = document.querySelector('tretij')
var location3 = document.createElement('P')
tretij.appendChild(location3)
location3.id = 'locationthird';
location3.innerHTML = "Earth (Replacement Dimension)"

// создание четвертого параграфа в третьей  карточке created

var par34 = document.querySelector('tretij')
var created3 = document.createElement('P')
tretij.appendChild(created3)
created3.id = 'createdthird';
created3.innerHTML = "2017-11-04T19:09:56.428Z"

// создание пятого параграфа в третьей карточке episode

var par35 = document.querySelector('tretij')
var episode3 = document.createElement('P')
tretij.appendChild(episode3)
episode3.id = 'episodethird';
episode3.innerHTML = "29"

// создание шестого параграфа в третьей карте - картинки 

var img3 = document.querySelector('tretij')
var foto3 = document.createElement('img')

tretij.appendChild(foto3)

foto3.id = 'fotothird';

foto3.innerHTML = "<img name='third' src='https://rickandmortyapi.com/api/character/avatar/3.jpeg' style='height:100%; width:100%;'>";


//СОЗДАНИЕ ЧЕТВЕРТОЙ КАРТЫ

var div4 = document.querySelector('CARDS')
var card4 = document.createElement('div')
// card4.className = 'card4'
CARDS.appendChild(card4)
card4.id = 'fourth';
card4.innerHTML = "This is the 4th card info"

//создание первого параграфа в 4-ой карте - имени

var par41 = document.querySelector('fourth')
var name41 = document.createElement('P')
fourth.appendChild(name41)
name41.id = 'namefourth';
name41.style.color = "pink"
name41.innerHTML = "Beth Smith"

// создание второго параграфа в 4-ой карточке species

var par42 = document.querySelector('fourth')
var species4 = document.createElement('P')
fourth.appendChild(species4)
species4.id = 'speciesfourth';
species4.innerHTML = "Human"

// создание третьего параграфа в 4-ой карточке location

var par43 = document.querySelector('fourth')
var location4 = document.createElement('P')
fourth.appendChild(location4)
location4.id = 'locationfourth';
location4.innerHTML = "Earth (Replacement Dimension)"

// создание четвертого параграфа в 4-ой карточке created

var par44 = document.querySelector('fourth')
var created4 = document.createElement('P')
fourth.appendChild(created4)
created4.id = 'createdfourth';
created4.innerHTML = "2017-11-04T19:22:43.665Z"

// создание пятого параграфа в 4-ой карточке episode

var par45 = document.querySelector('fourth')
var episode4 = document.createElement('P')
fourth.appendChild(episode4)
episode4.id = 'episodefourth';
episode4.innerHTML = "29"

// создание шестого параграфа в 4-ой карте - картинки 

var img4 = document.querySelector('fourth')
var foto4 = document.createElement('img')

fourth.appendChild(foto4)

foto4.id = 'fotofourth';

foto4.innerHTML = "<img name='fourth' src='https://rickandmortyapi.com/api/character/avatar/4.jpeg' style='height:100%; width:100%;'>";

//СОЗДАНИЕ ПЯТОЙ КАРТЫ

var div5 = document.querySelector('CARDS')
var card5 = document.createElement('div')
// card5.className = 'card5'
CARDS.appendChild(card5)
card5.id = 'fifth';
card5.innerHTML = "This is the 5th card info"

//создание первого параграфа в 5-ой карте - имени

var par51 = document.querySelector('fifth')
var name51 = document.createElement('P')
fifth.appendChild(name51)
name51.id = 'namefifth';
name51.style.color = "purple"
name51.innerHTML = "Jerry Smith"

// создание второго параграфа в 5-ой карточке species

var par52 = document.querySelector('fifth')
var species5 = document.createElement('P')
fifth.appendChild(species5)
species5.id = 'speciesfifth';
species5.innerHTML = "Human"

// создание третьего параграфа в 5-ой карточке location

var par53 = document.querySelector('fifth')
var location5 = document.createElement('P')
fifth.appendChild(location5)
location5.id = 'locationfifth';
location5.innerHTML = "Earth (Replacement Dimension)"

// создание четвертого параграфа в 5-ой карточке created

var par54 = document.querySelector('fifth')
var created5 = document.createElement('P')
fifth.appendChild(created5)
created5.id = 'createdfifth';
created5.innerHTML = "2017-11-04T19:26:56.301Z"

// создание пятого параграфа в 5-ой карточке episode

var par55 = document.querySelector('fifth')
var episode5 = document.createElement('P')
fifth.appendChild(episode5)
episode5.id = 'episodefifth';
episode5.innerHTML = "25"

// создание шестого параграфа в 5-ой карте - картинки 

var img5 = document.querySelector('fifth')
var foto5 = document.createElement('img')

fifth.appendChild(foto5)

foto5.id = 'fotofifth';

foto5.innerHTML = "<img name='fifth' src='https://rickandmortyapi.com/api/character/avatar/5.jpeg' style='height:100%; width:100%;'>";


//СОЗДАНИЕ ШЕСТОЙ КАРТЫ

var div6 = document.querySelector('CARDS')
var card6 = document.createElement('div')
// card6.className = 'card6'
CARDS.appendChild(card6)
card6.id = 'sixth';
card6.innerHTML = "This is the 6th card info"

//создание первого параграфа в 6-ой карте - имени

var par61 = document.querySelector('sixth')
var name61 = document.createElement('P')
sixth.appendChild(name61)
name61.id = 'namesixth';
name61.style.color = "yellow"
name61.innerHTML = "Abadango Cluster Princess"

// создание второго параграфа в 6-ой карточке species

var par62 = document.querySelector('sixth')
var species6 = document.createElement('P')
sixth.appendChild(species6)
species6.id = 'speciessixth';
species6.innerHTML = "Alien"

// создание третьего параграфа в 6-ой карточке location

var par63 = document.querySelector('sixth')
var location6 = document.createElement('P')
sixth.appendChild(location6)
location6.id = 'locationsixth';
location6.innerHTML = "Abadango"

// создание четвертого параграфа в 6-ой карточке created

var par64 = document.querySelector('sixth')
var created6 = document.createElement('P')
sixth.appendChild(created6)
created6.id = 'createdsixth';
created6.innerHTML = "2017-11-04T19:50:28.250Z"

// создание пятого параграфа в 6-ой карточке episode

var par65 = document.querySelector('sixth')
var episode6 = document.createElement('P')
sixth.appendChild(episode6)
episode6.id = 'episodesixth';
episode6.innerHTML = "1"

// создание шестого параграфа в 6-ой карте - картинки 

var img6 = document.querySelector('sixth')
var foto6 = document.createElement('img')

sixth.appendChild(foto6)

foto6.id = 'fotosixth';

foto6.innerHTML = "<img name='sixth' src='https://rickandmortyapi.com/api/character/avatar/6.jpeg' style='height:100%; width:100%;'>";


//СОЗДАНИЕ СЕДЬМОЙ КАРТЫ

var div7 = document.querySelector('CARDS')
var card7 = document.createElement('div')
// card7.className = 'card7'
CARDS.appendChild(card7)
card7.id = 'seventh';
card7.innerHTML = "This is the 7th card info"

//создание первого параграфа в 7-ой карте - имени

var par71 = document.querySelector('seventh')
var name71 = document.createElement('P')
seventh.appendChild(name71)
name71.id = 'nameseventh';
name71.style.color = "brown"
name71.innerHTML = "Abradolf Lincler"

// создание второго параграфа в 7-ой карточке species

var par72 = document.querySelector('seventh')
var species7 = document.createElement('P')
seventh.appendChild(species7)
species7.id = 'speciesseventh';
species7.innerHTML = "Human"

// создание третьего параграфа в 7-ой карточке location

var par73 = document.querySelector('seventh')
var location7 = document.createElement('P')
seventh.appendChild(location7)
location7.id = 'locationseventh';
location7.innerHTML = "Testicle Monster Dimension" //...<= яички...oops

// создание четвертого параграфа в 7-ой карточке created

var par74 = document.querySelector('seventh')
var created7 = document.createElement('P')
seventh.appendChild(created7)
created7.id = 'createdseventh';
created7.innerHTML = "2017-11-04T19:59:20.523Z"

// создание пятого параграфа в 7-ой карточке episode

var par75 = document.querySelector('seventh')
var episode7 = document.createElement('P')
seventh.appendChild(episode7)
episode7.id = 'episodeseventh';
episode7.innerHTML = "2"

// создание шестого параграфа в 7-ой карте - картинки 

var img7 = document.querySelector('seventh')
var foto7 = document.createElement('img')

seventh.appendChild(foto7)

foto7.id = 'fotoseventh';

foto7.innerHTML = "<img name='seventh' src='https://rickandmortyapi.com/api/character/avatar/7.jpeg' style='height:100%; width:100%;'>";

//СОЗДАНИЕ ВОСЬМОЙ КАРТЫ

var div8 = document.querySelector('CARDS')
var card8 = document.createElement('div')
// card8.className = 'card8'
CARDS.appendChild(card8)
card8.id = 'eighth';
card8.innerHTML = "This is the 8th card info"

//создание первого параграфа в 8-ой карте - имени

var par81 = document.querySelector('eighth')
var name81 = document.createElement('P')
eighth.appendChild(name81)
name81.id = 'nameeighth';
name81.style.color = "lime"
name81.innerHTML = "Adjudicator Rick"

// создание второго параграфа в 8-ой карточке species

var par82 = document.querySelector('eighth')
var species8 = document.createElement('P')
eighth.appendChild(species8)
species8.id = 'specieseighth';
species8.innerHTML = "Human"

// создание третьего параграфа в 8-ой карточке location

var par83 = document.querySelector('eighth')
var location8 = document.createElement('P')
eighth.appendChild(location8)
location8.id = 'locationeighth';
location8.innerHTML = "Earth (Replacement Dimension)" 

// создание четвертого параграфа в 8-ой карточке created

var par84 = document.querySelector('eighth')
var created8 = document.createElement('P')
eighth.appendChild(created8)
created8.id = 'createdeighth';
created8.innerHTML = "2017-11-04T20:03:34.737Z"

// создание пятого параграфа в 8-ой карточке episode

var par85 = document.querySelector('eighth')
var episode8 = document.createElement('P')
eighth.appendChild(episode8)
episode8.id = 'episodeeighth';
episode8.innerHTML = "1"

// создание шестого параграфа в 8-ой карте - картинки 

var img8 = document.querySelector('eighth')
var foto8 = document.createElement('img')

eighth.appendChild(foto8)

foto8.id = 'fotoeighth';

foto8.innerHTML = "<img name='eighth' src='https://rickandmortyapi.com/api/character/avatar/8.jpeg' style='height:100%; width:100%;'>";



//СОЗДАНИЕ ДЕВЯТОЙ КАРТЫ

var div9 = document.querySelector('CARDS')
var card9 = document.createElement('div')
// card9.className = 'card9'
CARDS.appendChild(card9)
card9.id = 'ninth';
card9.innerHTML = "This is the 9th card info"

//создание первого параграфа в 9-ой карте - имени

var par91 = document.querySelector('ninth')
var name91 = document.createElement('P')
ninth.appendChild(name91)
name91.id = 'nameninth';
name91.style.color = "aqua"
name91.innerHTML = "Agency Director"

// создание второго параграфа в 9-ой карточке species

var par92 = document.querySelector('ninth')
var species9 = document.createElement('P')
ninth.appendChild(species9)
species9.id = 'speciesninth';
species9.innerHTML = "Human"

// создание третьего параграфа в 9-ой карточке location

var par93 = document.querySelector('ninth')
var location9 = document.createElement('P')
ninth.appendChild(location9)
location9.id = 'locationninth';
location9.innerHTML = "Earth (Replacement Dimension)" 

// создание четвертого параграфа в 9-ой карточке created

var par94 = document.querySelector('ninth')
var created9 = document.createElement('P')
ninth.appendChild(created9)
created9.id = 'createdninth';
created9.innerHTML = "2017-11-04T20:06:54.976Z"

// создание пятого параграфа в 9-ой карточке episode

var par95 = document.querySelector('ninth')
var episode9 = document.createElement('P')
ninth.appendChild(episode9)
episode9.id = 'episodeninth';
episode9.innerHTML = "1"

// создание шестого параграфа в 9-ой карте - картинки 

var img9 = document.querySelector('ninth')
var foto9 = document.createElement('img')

ninth.appendChild(foto9)

foto9.id = 'fotoninth';

foto9.innerHTML = "<img name='ninth' src='https://rickandmortyapi.com/api/character/avatar/9.jpeg' style='height:100%; width:100%;'>";


//СОЗДАНИЕ ДЕСЯТОЙ КАРТЫ

var div10 = document.querySelector('CARDS')
var card10 = document.createElement('div')
// card10.className = 'card10'
CARDS.appendChild(card10)
card10.id = 'tenth';
card10.innerHTML = "This is the 10th card info"

//создание первого параграфа в 10-ой карте - имени

var par101 = document.querySelector('tenth')
var name101 = document.createElement('P')
tenth.appendChild(name101)
name101.id = 'nametenth';
name101.style.color = "red"
name101.innerHTML = "Alan Rails"

// создание второго параграфа в 10-ой карточке species

var par102 = document.querySelector('tenth')
var species10 = document.createElement('P')
tenth.appendChild(species10)
species10.id = 'speciestenth';
species10.innerHTML = "Human"

// создание третьего параграфа в 10-ой карточке location

var par103 = document.querySelector('tenth')
var location10 = document.createElement('P')
tenth.appendChild(location10)
location10.id = 'locationtenth';
location10.innerHTML = "Worldender's lair" 

// создание четвертого параграфа в 10-ой карточке created

var par104 = document.querySelector('tenth')
var created10 = document.createElement('P')
tenth.appendChild(created10)
created10.id = 'createdtenth';
created10.innerHTML = "2017-11-04T20:19:09.017Z"

// создание пятого параграфа в 10-ой карточке episode

var par105 = document.querySelector('tenth')
var episode10 = document.createElement('P')
tenth.appendChild(episode10)
episode10.id = 'episodetenth';
episode10.innerHTML = "1"

// создание шестого параграфа в 10-ой карте - картинки 

var img10 = document.querySelector('tenth')
var foto10 = document.createElement('img')

tenth.appendChild(foto10)

foto10.id = 'fototenth';

foto10.innerHTML = "<img name='tenth' src='https://rickandmortyapi.com/api/character/avatar/10.jpeg' style='height:100%; width:100%;'>";

